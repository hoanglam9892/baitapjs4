/* 
Excercise 1
Đầu vào: 3 số nguyên
Các bước xử lý: 
1. Tìm max, min, sau đó tìm mid
2. Max: Gắn max = số 1, so sánh lần lượt max với số 2 và số 3.
3. Min: Tương tự max
4. Mid: Gắn mid = số 1, nếu số 2 hoặc số 3 thỏa 2 điều kiện < max hoặc >min thì gắn mid cho số đó.
Đầu ra: 3 số nguyên theo thứ tự tăng dần
*/

function exercise1(){
    var so1 = document.getElementById("ex1-so1").value*1;
    var so2 = document.getElementById("ex1-so2").value*1;
    var so3 = document.getElementById("ex1-so3").value*1;
    var max=so1;
    var min=so1;
    var mid=so1;
    var ex1_thongBao = document.getElementById("ex1-xuat");

    if(so2>max){
        max=so2;
    };
    if(so3>max){
        max=so3;
    };

    if(so2<min){
        min=so2;
    };
    if(so3<min){
        min=so3;
    };

    if(so2<max && so2>min){
        mid=so2;
    };
    if(so3<max && so3>min){
        mid=so3;
    };

    ex1_thongBao.innerText=`3 số nguyên theo thứ tự tăng dần: ${min}, ${mid}, ${max}`;

};

/* 
Excercise 2
Đầu vào: Chọn ai là người sử dụng máy
Các bước xử lý: 
Đặt value bên file html, sau đó so sánh giá trị bên file js rồi viết các dòng thông báo tương ứng
Đầu ra: Chào người sử dụng máy
*/

function exercise2(){
    var who = document.getElementById("ex2-select").value;
    var ex2_thongBao = document.getElementById("ex2-chao");

    if(who == "B"){
        ex2_thongBao.innerText=`👉 Chào Bố`;
    } else if(who == "M"){
        ex2_thongBao.innerText=`👉 Chào Mẹ`;
    } else if(who == "A"){
        ex2_thongBao.innerText=`👉 Chào Anh trai`;
    } else if(who == "E"){
        ex2_thongBao.innerText=`👉 Chào Em gái`;
    } else{
        ex2_thongBao.innerText=`👉 Chào Người lạ!!!`;
    }
};

/* 
Excercise 3
Đầu vào: Chọn ai là người sử dụng máy
Các bước xử lý: 
Đặt value bên file html, sau đó so sánh giá trị bên file js rồi viết các dòng thông báo tương ứng
Đầu ra: Chào người sử dụng máy
*/

function exercise3(){
    var so1 = document.getElementById("ex3-so1").value*1;
    var so2 = document.getElementById("ex3-so2").value*1;
    var so3 = document.getElementById("ex3-so3").value*1;
    var ex3_thongBao = document.getElementById("ex3-dem");
    var demChan = 0;
    var demLe = 0;


    if(so1%2 == 0){
        demChan++;
    } else{
        demLe++;
    };

    if(so2%2 == 0){
        demChan++;
    } else{
        demLe++;
    };

    if(so3%2 == 0){
        demChan++;
    } else{
        demLe++;
    };

    ex3_thongBao.innerText=`👉 Có ${demChan} số chắn và ${demLe} số lẻ.`;
};


/* 
Excercise 4
Đầu vào: 3 cạnh của tam giác
Các bước xử lý: 
1. Kiểm tra có phải là hình tam giác không? Nếu tổng của 2 số bất kì bé hơn số còn lại thì yêu cầu nhập lại.
2. Nếu 3 cạnh bằng nhau thì là tam giác đều, 2 cạnh bằng nhau thì là tam giác cân, tổng của bình phương 2 cạnh bất kì bằng bình phương cạnh còn lại thì là tam giác vuông, nếu không nằm trong 3 trường hợp trên thì là loại tam giác khác.
Đầu ra: Cho biết đó là tam giác gì
*/

function exercise4(){
    var so1 = document.getElementById("ex4-so1").value*1;
    var so2 = document.getElementById("ex4-so2").value*1;
    var so3 = document.getElementById("ex4-so3").value*1;
    var ex4_thongBao = document.getElementById("ex4-du-doan");

    if((so1+so2<so3) || (so2+so3<so1) || (so1+so3<so2)){
        ex4_thongBao.innerText=`👉 Dữ liệu không hợp lệ, vui lòng nhập lại!`;
    } else if(so1 == so2 && so2 == so3){
        ex4_thongBao.innerText=`👉 Đây là tam giác đều`;
    } else if((so1 == so2) || (so2 == so3) || (so1 == so3)){
        ex4_thongBao.innerText=`👉 Đây là tam giác cân`;
    } else if((so1*so1 == so2*so2 + so3*so3) || (so1*so1 + so2*so2 == so3*so3) || (so1*so1 + so3*so3 == so2*so2)){
        ex4_thongBao.innerText=`👉 Đây là tam giác vuông`;
    } else{
        ex4_thongBao.innerText=`👉 Đây là một loại tam giác khác`;
    }
};



